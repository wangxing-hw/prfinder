mod gitee;

use crate::gitee::api::{ApiPullRequestInfo, GiteeApi, PullRequestsChangsSummary};
use clap::Parser;
use clap::ValueEnum;
use csv::Writer;
use std::fmt;
use tokio::task;
/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[arg(short, long)]
    token: String,

    /// State of pull requests
    #[clap(value_enum,short,long,default_value_t=PullRequestsState::Open)]
    state: PullRequestsState,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum PullRequestsState {
    /// Fetch merged pull requests only.
    Merged,

    /// fetch open pull requests only.
    Open,

    /// fetch clased pull requests only.
    Closed,

    /// fetch all pull requests.
    All,
}

impl fmt::Display for PullRequestsState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

async fn fill_all_pr_info(
    api: &GiteeApi,
    all_pr: &Vec<ApiPullRequestInfo>,
) -> Vec<PullRequestsChangsSummary> {
    let mut all_pr_info = Vec::<PullRequestsChangsSummary>::new();
    let mut set = task::JoinSet::new();

    for pr_group in all_pr.chunks(20) {
        for pr in pr_group {
            let api = api.clone();
            let pr = pr.clone();
            set.spawn(async move {
                let pr_info = api.fetch_all_pr_info(&pr).await;
                pr_info
            });
        }
        while let Some(res) = set.join_next().await {
            let pr = res.unwrap();
            //println!("{:?}", idx);
            all_pr_info.push(pr);
        }
    }
    all_pr_info
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let state = args.state;

    let api = GiteeApi::new(&args.token);
    let mut all_pr = Vec::<ApiPullRequestInfo>::new();

    let mut set = task::JoinSet::new();

    let pages = api
        .get_enterprise_one_page_count("open_harmony", &state.to_string().to_lowercase())
        .await
        .unwrap_or(0);

    println!("Total pages: {}", pages);
    let pages: Vec<i32> = (1..pages).collect();

    for chunk in pages.chunks(50) {
        for page in chunk {
            let api = api.clone();
            let page = page.clone();
            set.spawn(async move {
                let page_pr = api
                    .get_enterprise_one_page_pull_requests(
                        "open_harmony",
                        &state.to_string().to_lowercase(),
                        page,
                    )
                    .await;
                page_pr
            });
        }
        while let Some(res) = set.join_next().await {
            let page_pr = res.unwrap();
            all_pr.extend(page_pr);
        }
    }

    let all_info = fill_all_pr_info(&api, &all_pr).await;
    let mut writer = Writer::from_path("out.csv").unwrap();

    for info in all_info {
        writer.serialize(info).unwrap();
    }
    writer.flush().unwrap();
}
