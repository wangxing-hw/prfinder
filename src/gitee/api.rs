use serde::Deserialize;
use serde::Serialize;
use serde_json;
use std::collections::HashSet;
use std::ffi::OsStr;
use std::path::Path;

#[derive(Deserialize, Debug, Clone)]
pub struct ApiPullRequestInfo {
    html_url: String,
    url: String,
    created_at: String,
    title: String,
    state: String,
}

#[derive(Deserialize, Debug, Clone)]
struct ApiPullRequestFiles {
    additions: String,
    deletions: String,
    filename: String,
}

#[derive(Serialize)]
pub struct PullRequestsChangsSummary {
    url: String,
    title: String,
    state: String,
    add_lines: i32,
    del_lines: i32,
    language: String,
    created_at: String,
}
#[derive(Debug, Clone)]
pub struct GiteeApi {
    api_token: String,
}

fn get_extension_from_filename(filename: &str) -> Option<&str> {
    Path::new(filename).extension().and_then(OsStr::to_str)
}

fn get_lang_from_filename(filename: &str) -> Option<&str> {
    let ext_option = get_extension_from_filename(filename);
    if ext_option == None {
        return None;
    }
    let ext = ext_option.unwrap();
    match ext {
        "c" => Some("C"),
        "cpp" => Some("C++"),
        "java" => Some("Java"),
        "js" => Some("JavaScript"),
        "php" => Some("PHP"),
        "py" => Some("Python"),
        "rb" => Some("Ruby"),
        "sh" => Some("Bash"),
        "ts" => Some("TypeScript"),
        "ets" => Some("Extended TypeScript"),
        "gn" => Some("Build Script"),
        "md" => Some("Markdown"),
        _ => None,
    }
}

impl GiteeApi {
    pub fn new(api_token: &str) -> Self {
        GiteeApi {
            api_token: String::from(api_token),
        }
    }

    pub async fn get_enterprise_one_page_count(
        &self,
        enterprise: &str,
        state: &str,
    ) -> Option<i32> {
        let api_url = format!(
            "https://gitee.com/api/v5/enterprise/{}/pull_requests",
            enterprise
        );
        let client = reqwest::Client::builder().build().unwrap();

        // Perform the actual execution of the network request
        let res = client
            .get(api_url)
            .query(&[
                ("access_token", &self.api_token),
                ("per_page", &String::from("100")),
                ("state", &String::from(state)),
            ])
            .send()
            .await
            .unwrap();

        if let Some(total) = res.headers().get("total_page") {
            Some(total.to_str().unwrap().parse::<i32>().unwrap())
        } else {
            None
        }
    }

    pub async fn get_enterprise_one_page_pull_requests(
        &self,
        enterprise: &str,
        state: &str,
        page: i32,
    ) -> Vec<ApiPullRequestInfo> {
        println!("Fetching page: {}", page);
        let api_url = format!(
            "https://gitee.com/api/v5/enterprise/{}/pull_requests",
            enterprise
        );
        let client = reqwest::Client::builder().build().unwrap();

        // Perform the actual execution of the network request
        let res = client
            .get(api_url)
            .query(&[
                ("access_token", &self.api_token),
                ("per_page", &String::from("100")),
                ("page", &page.to_string()),
                ("state", &String::from(state)),
                ("direction", &String::from("desc")),
                ("sort", &String::from("created")),
            ])
            .send()
            .await
            .unwrap();

        let prs = res.json::<Vec<ApiPullRequestInfo>>().await.unwrap();
        prs
    }

    pub async fn fetch_all_pr_info(
        &self,
        api_info: &ApiPullRequestInfo,
    ) -> PullRequestsChangsSummary {
        println!("Fetching PR: {}", api_info.html_url);
        let changes_api_url = format!("{}/files", api_info.url);
        let client = reqwest::Client::builder().build().unwrap();

        // Perform the actual execution of the network request
        let body = client
            .get(changes_api_url)
            .query(&[("access_token", &self.api_token)])
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap();

        let files = serde_json::from_str(&body);
        match files {
            Ok(files) => fill_pull_request_summary(api_info, &files),
            Err(e) => {
                e.to_string();
                println!("{:?}", body);
                panic!();
            }
        }
    }
}

fn fill_pull_request_summary(
    api_info: &ApiPullRequestInfo,
    files: &Vec<ApiPullRequestFiles>,
) -> PullRequestsChangsSummary {
    let mut add = 0;
    let mut del = 0;
    let mut langs = HashSet::<String>::new();
    for file in files {
        add += file.additions.parse::<i32>().unwrap();
        del += file.deletions.parse::<i32>().unwrap();
        let filetype_option = get_lang_from_filename(&file.filename);
        if let Some(filetype) = filetype_option {
            langs.insert(filetype.to_string());
        }
    }
    let v = Vec::from_iter(langs);
    PullRequestsChangsSummary {
        add_lines: add,
        del_lines: del,
        url: api_info.html_url.clone(),
        language: v.join("|"),
        created_at: api_info.created_at.clone(),
        state: api_info.state.clone(),
        title: api_info.title.clone(),
    }
}
