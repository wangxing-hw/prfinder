# prfinder

#### 介绍
OpenHarmony PR 数据获取工具

#### 软件架构

使用 gitee api 获取对应数据 [API文档](http://https://gitee.com/api/v5/swagge)

#### 安装教程


```bash
cargo build

```

#### 使用说明



```
prfinder --token <TOKEN> --status [STATUS]

TOKEN: gitee api token
STATUS: PR status: Open,Close,Merged,All

```




